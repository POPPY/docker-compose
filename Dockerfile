FROM docker

RUN apk add --no-cache py-pip python3-dev libffi-dev openssl-dev gcc libc-dev rust cargo make
RUN apk add --no-cache poetry uv
RUN apk add --no-cache docker-compose
RUN apk update py-pip
RUN docker compose version
