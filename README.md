## Docker and Docker Compose
The `latest` tag is currently:

- docker: latest
- docker-compose: latest


### Use as base image

Login to the gitlab-hosted registry:

```
docker login gitlab.obspm.fr:4567
```

And use the docker-compose image like any other docker image

```Dockerfile
FROM gitlab.obspm.fr:4567/poppy/docker-compose:latest
```
